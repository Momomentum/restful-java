package de.htwBerlin.ai.kbe.songsRX.service;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import de.htwBerlin.ai.kbe.songsRX.bean.User;
import de.htwBerlin.ai.kbe.songsRX.storage.IUserDAO;
import de.htwBerlin.ai.kbe.songsRX.storage.TokenStorageInterface;


@Path("/auth")
public class AuthService {
	
	@Inject
	private TokenStorageInterface tokenStorage;
	
	@Inject
	private IUserDAO dao;
	
	@GET
    @Produces("text/plain")
	public Response getUser(@QueryParam("userId") String userId, @Context HttpServletRequest req) {
		User user = dao.findUserByUserId(userId);
		if (user != null) {
//			TokenStorage tokenStorage = TokenStorage.getInstance();
			String token = tokenStorage.updateUserToken(userId);
		    return Response.ok(token).build();
		} else {
		    return Response.status(Response.Status.FORBIDDEN).entity("Bad Credentials").build();
		}
	}
}
