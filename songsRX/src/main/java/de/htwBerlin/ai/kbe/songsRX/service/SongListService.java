package de.htwBerlin.ai.kbe.songsRX.service;

import java.util.Collection;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import de.htwBerlin.ai.kbe.songsRX.bean.SongList;
import de.htwBerlin.ai.kbe.songsRX.storage.ISongListDAO;


@Path("/songLists")
public class SongListService {

    private ISongListDAO songListDao;

    @Inject
    public SongListService(ISongListDAO dao) {
        this.songListDao = dao;
    }
    
	@GET 
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Collection<SongList> getAllSongLists() {
		Collection<SongList> songLists = songListDao.findAllSongLists();
		return songLists;
	}
	
	
}
