package de.htwBerlin.ai.kbe.songsRX.bean;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
@Table(name="songlist")
@XmlRootElement(name = "songList")
public class SongList{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=User.class)
	@JoinColumn(name="user_id")
	private User user; 
	
	@Column(name="visibility")
	private boolean visibility;
	
    @ManyToMany(targetEntity=Song.class, fetch=FetchType.EAGER)
        @JoinTable(name = "song_songlist",
            joinColumns = @JoinColumn(name = "songlist_id"),
            inverseJoinColumns = @JoinColumn(name = "song_id")
        )
	private List<Song> songs = new ArrayList<>();
	
	public SongList () {}

	@XmlElement
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Transient
	@XmlTransient
	public User getUser() {
		return user;
	}
	
	@XmlElement
	public boolean getVisibility() {
		return visibility;
	}

	@XmlElementWrapper(name="songs")
	@XmlElement(name="song")
	@JsonProperty("songs")
	public List<Song> getSongs() {
		return songs;
	}

	public void addSong(Song song) {
		songs.add(song);
	}
	
	public void removeSong(Song song) {
		songs.remove(song);
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}
}
