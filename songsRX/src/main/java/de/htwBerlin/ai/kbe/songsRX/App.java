package de.htwBerlin.ai.kbe.songsRX;
import org.glassfish.jersey.server.ResourceConfig;

import de.htwBerlin.ai.kbe.songsRX.di.DependencyBinder;
import de.htwBerlin.ai.kbe.songsRX.filter.AuthenticationFilter;

public class App extends ResourceConfig {
	
	    public App() {
	        register(new DependencyBinder());
	    	packages("de.htwBerlin.ai.kbe.songsRX.service");
	    	register(AuthenticationFilter.class);
	    }

}
