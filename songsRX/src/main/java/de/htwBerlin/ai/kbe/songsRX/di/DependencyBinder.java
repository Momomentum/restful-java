package de.htwBerlin.ai.kbe.songsRX.di;

import javax.inject.Singleton;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

import de.htwBerlin.ai.kbe.songsRX.storage.*;


public class DependencyBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(TokenStorage.class).to(TokenStorageInterface.class).in(Singleton.class);
        bind(SongDAO.class).to(ISongDAO.class).in(Singleton.class);
        bind(UserDAO.class).to(IUserDAO.class).in(Singleton.class);
        bind(SongListDAO.class).to(ISongListDAO.class).in(Singleton.class);
    }
}