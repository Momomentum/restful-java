package de.htwBerlin.ai.kbe.songsRX.storage;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import de.htwBerlin.ai.kbe.songsRX.bean.Song;
import de.htwBerlin.ai.kbe.songsRX.bean.SongList;
import de.htwBerlin.ai.kbe.songsRX.bean.User;

public class SongListDAO implements ISongListDAO{
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("songsRX-U");

    @Override
    public SongList findSongListById(Integer id) {
        EntityManager em = emf.createEntityManager();
        SongList entity = null;
        try {
            entity = em.find(SongList.class, id);
        } finally {
            em.close();
        }
        return entity;
    }
    
    @Override
    public SongList findPublicSongListById(Integer id) {
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<SongList> query = em.createQuery("SELECT s FROM SongList s WHERE s.visibility = true and s.id = :id", SongList.class);
            query.setParameter("id", id);
            return (SongList) query.getSingleResult();
        } catch(NoResultException e) {
        	return null;
        }
        finally {
            em.close();
        }
    }

    @Override
    public Collection<SongList> findAllSongLists() {
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<SongList> query = em.createQuery("SELECT s FROM SongList s", SongList.class);
            return query.getResultList();
        } finally {
            em.close();
        }
    }
    
    @Override
    public Collection<SongList> findAllPublicSongLists() {
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<SongList> query = em.createQuery("SELECT s FROM SongList s WHERE s.visibility = true", SongList.class);
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public Collection<SongList> findAllPublicSongListsOfUser(User user) {
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<SongList> query = em.createQuery("SELECT s FROM SongList s WHERE s.visibility = true and s.user = :user", SongList.class);
            query.setParameter("user", user);
            return query.getResultList();
        } finally {
            em.close();
        }
    }
    
    @Override
    public Collection<SongList> findAllSongListsOfUser(User user) {
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<SongList> query = em.createQuery("SELECT s FROM SongList s WHERE s.user = :user", SongList.class);
            query.setParameter("user", user);
            return query.getResultList();
        } finally {
            em.close();
        }
    }
    
    
    @Override
    public Integer saveSongList(SongList songList) throws PersistenceException {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try {
            transaction.begin();
            em.persist(songList);
            transaction.commit();
            return songList.getId();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error adding songList: " + e.getMessage());
            transaction.rollback();
            throw new PersistenceException("Could not persist entity: " + e.toString());
        } finally {
            em.close();
        }
    }

    @Override
    public void updateSongList(SongList songList) throws PersistenceException {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        SongList oldSongList = em.find(SongList.class, songList.getId());
        try {
            transaction.begin();
            em.detach(oldSongList);
            em.merge(songList);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error updating song: " + e.getMessage());
            transaction.rollback();
            throw new PersistenceException("Could not persist entity: " + e.toString());
        } finally {
            em.close();
        }
    }
    
    @Override
    public void deleteSongList(Integer id) throws PersistenceException {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        SongList songList = null;
        try {
            songList = em.find(SongList.class, id);
            if (songList != null) {
                System.out.println("Deleting: " + songList.getId() + " with id: " + songList.getId());
                transaction.begin();
                em.remove(songList);
                transaction.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error removing songList: " + e.getMessage());
            transaction.rollback();
            throw new PersistenceException("Could not remove entity: " + e.toString());
        } finally {
            em.close();
        }
    }
}
