package de.htwBerlin.ai.kbe.songsRX.storage;

import java.util.Collection;

import javax.persistence.PersistenceException;

import de.htwBerlin.ai.kbe.songsRX.bean.SongList;
import de.htwBerlin.ai.kbe.songsRX.bean.User;

public interface ISongListDAO {
    /**
     * Retrieves a songList
     * 
     * @param id
     * @return
     */
    public SongList findSongListById(Integer id);
    
    public SongList findPublicSongListById(Integer id);

    /**
     * Retrieves all songLists
     * 
     * @return
     */
    public Collection<SongList> findAllSongLists();

    
    public Collection<SongList> findAllPublicSongLists();
    
	Collection<SongList> findAllPublicSongListsOfUser(User user);

    
    /**
     * Save a new songLists
     * 
     * @param songList
     * @return the Id of the new songList
     */
    public Integer saveSongList(SongList songList);
    
    /**
     * Deletes the songList for the provided id
     * @param id
     */
    public void deleteSongList(Integer id);

	void updateSongList(SongList songList) throws PersistenceException;

	Collection<SongList> findAllSongListsOfUser(User user);

    


}
