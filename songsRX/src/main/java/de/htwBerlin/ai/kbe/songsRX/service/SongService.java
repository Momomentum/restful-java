package de.htwBerlin.ai.kbe.songsRX.service;

import java.net.URI;
import java.util.Collection;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.message.internal.MessageBodyProviderNotFoundException;

import de.htwBerlin.ai.kbe.songsRX.bean.Song;
import de.htwBerlin.ai.kbe.songsRX.storage.ISongDAO;
import de.htwBerlin.ai.kbe.songsRX.storage.SongStore;


@Path("/songs")
public class SongService {
	
    private ISongDAO songDao;

    @Inject
    public SongService(ISongDAO dao) {
        this.songDao = dao;
    }
	
	@GET 
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Collection<Song> getAllSongs() {
		Collection<Song> songs = songDao.findAllSongs();
		return songs;
	}
	
	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getSong(@PathParam("id") Integer id) {
		Song song = songDao.findSongById(id);
		if(song == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		return Response.ok().entity(song).build();
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces(MediaType.TEXT_PLAIN)
	public Response createSong(Song song)  {
		try {
			int id = songDao.saveSong(song);
			return Response.created(URI.create("/songs/" + Integer.toString(id))).entity(id).build();

		} catch (PersistenceException e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	
	
	@PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("/{id}")
    public Response updateSong(@PathParam("id") Integer id, Song song) {
		if(songDao.findSongById(id) == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		try {
			song.setId(id);
			songDao.updateSong(song);
			return Response.status(Response.Status.NO_CONTENT).build();
		} catch (PersistenceException e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
    }
	
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Integer id) {
		if(songDao.findSongById(id) == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		try {
			songDao.deleteSong(id);
			return Response.status(Response.Status.NO_CONTENT).build();
		} catch (PersistenceException e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}



}
