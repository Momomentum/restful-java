package de.htwBerlin.ai.kbe.songsRX.storage;

import java.util.Collection;

import de.htwBerlin.ai.kbe.songsRX.bean.Song;

public interface ISongDAO {

    /**
     * Retrieves a song
     * 
     * @param id
     * @return
     */
    public Song findSongById(Integer id);

    /**
     * Retrieves all songs
     * 
     * @return
     */
    public Collection<Song> findAllSongs();

    /**
     * Save a new song
     * 
     * @param song
     * @return the Id of the new songs
     */
    public Integer saveSong(Song song);

    /**
     * Update song
     * 
     * @param song
     */
    public void updateSong(Song song);
    
    
    /**
     * Deletes the song for the provided id
     * @param id
     */
    public void deleteSong(Integer id);
	
}
