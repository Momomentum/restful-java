package de.htwBerlin.ai.kbe.songsRX.storage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.htwBerlin.ai.kbe.songsRX.bean.Song;

public class SongStore {

	private static Map<Integer,Song> storage;
	private static SongStore instance = null;
	private static ObjectMapper mapper = null;	
	private static AtomicInteger highestId = null;

	private SongStore() {
		storage = new HashMap<Integer,Song>();
	}
	
	public synchronized static SongStore getInstance() {
		if (instance == null) {
			instance = new SongStore();
		    mapper = new ObjectMapper();
		    InputStream input = instance.getClass().getClassLoader().getResourceAsStream("songs.json");
		    SongStore songStore = SongStore.getInstance();
		    highestId = new AtomicInteger(0);
		    
		    
			List<Song> songList;
			try {
				songList = (List<Song>) mapper.readValue(input, new TypeReference<List<Song>>(){});
				for (Song song : songList) {
					if(song.getId() > highestId.get()) {
						highestId.set(song.getId());
					}
					instance.addSong(song);
				}
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	public Song getSong(Integer id) {
		return storage.get(id);
	}
	
	public Collection<Song> getAllSongs() {
		return storage.values();
	}
	
	public Integer addNewSong(Song song) {
		song.setId(highestId.incrementAndGet());
		storage.put(song.getId(), song);
		return song.getId();	
	}
	
	
	public Integer addSong(Song song) {
		storage.put(song.getId(), song);
		return song.getId();
	}
	
	public Integer updateSong(Song song) {
		storage.put(song.getId(), song);
		return song.getId();	
	}
	
	// returns deleted song
	public Integer deleteSong(Integer id) {
		storage.remove(id);
		return id;
	}
}
