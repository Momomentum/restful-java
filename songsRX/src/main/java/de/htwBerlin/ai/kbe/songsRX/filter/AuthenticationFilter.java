package de.htwBerlin.ai.kbe.songsRX.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import de.htwBerlin.ai.kbe.songsRX.storage.TokenStorageInterface;

@Provider
public class AuthenticationFilter implements ContainerRequestFilter{


	public static final String AUTHENTICATION_HEADER = "Authorization";

	@Inject
	private TokenStorageInterface tokenStorage;
	
	@Override
	public void filter(ContainerRequestContext containerRequest) throws IOException {
		
		String authToken = containerRequest.getHeaderString(AUTHENTICATION_HEADER);

		
		if (authToken == null) {
			// etwas zu einfach: wenn "auth" in der URL, dann durchlassen:
			if (!containerRequest.getUriInfo().getPath().contains("auth")) { //kein "auth"
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}
		} else {
			if (!tokenStorage.authenticate(authToken)) { // Service kennt den Token nicht
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}
		}
	}

}
