package de.htwBerlin.ai.kbe.songsRX.storage;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import de.htwBerlin.ai.kbe.songsRX.bean.User;

public class UserDAO implements IUserDAO {
	
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("songsRX-U");

    @Override
    public User findUserById(Integer id) {
        EntityManager em = emf.createEntityManager();
        User entity = null;
        try {
            entity = em.find(User.class, id);
        } finally {
            em.close();
        }
        return entity;
    }
    
    @Override
    public User findUserByUserId(String userId) {
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<User> query = em.createQuery("SELECT s FROM User s where s.userId = :userId", User.class);
            return query.setParameter("userId", userId).getSingleResult();
        } catch (NoResultException e) {
			return null;
		}
        finally {
            em.close();
        }
    }

    @Override
    public Collection<User> findAllUsers() {
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<User> query = em.createQuery("SELECT s FROM User s", User.class);
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public Integer saveUser(User user) throws PersistenceException {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try {
            transaction.begin();
            em.persist(user);
            transaction.commit();
            return user.getId();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error adding user: " + e.getMessage());
            transaction.rollback();
            throw new PersistenceException("Could not persist entity: " + e.toString());
        } finally {
            em.close();
        }
    }

    @Override
    public void deleteUser(Integer id) throws PersistenceException {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        User user = null;
        try {
            user = em.find(User.class, id);
            if (user != null) {
                System.out.println("Deleting: " + user.getId() + " with Last Name: " + user.getLastName());
                transaction.begin();
                em.remove(user);
                transaction.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error removing user: " + e.getMessage());
            transaction.rollback();
            throw new PersistenceException("Could not remove entity: " + e.toString());
        } finally {
            em.close();
        }
    }
}
