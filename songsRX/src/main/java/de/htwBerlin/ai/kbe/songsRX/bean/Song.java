package de.htwBerlin.ai.kbe.songsRX.bean;


import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "song")
@Entity
@Table(name="song")
public class Song {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull
	@Column(name="title")
	private String title;
	
	@Column(name="artist")
	private String artist;
	
	@Column(name="album")
	private String album;
	
	@Column(name="released")
	private Integer released;
	
    @ManyToMany(mappedBy="songs", targetEntity=SongList.class, fetch=FetchType.EAGER)
	private List<SongList> songLists = new ArrayList<>();
	
	public Song () {}
	
	
	@XmlElement
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@XmlElement
	public String getTitle() {
		return title;
	}
	
	@XmlElement
	public String getArtist() {
		return artist;
	}
	
	@XmlElement
	public String getAlbum() {
		return album;
	}
	
	@XmlElement
	public Integer getReleased() {
		return released;
	}
	
	@Transient
	@XmlTransient
	public List<SongList> getSongLists() {
		return songLists;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public void setArtist(String artist) {
		this.artist = artist;
	}


	public void setAlbum(String album) {
		this.album = album;
	}


	public void setReleased(Integer released) {
		this.released = released;
	}


//	public void addSongList(SongList songList) {
//		songLists.add(songList);
//		songList.addSong(this);
//	}
	
}
