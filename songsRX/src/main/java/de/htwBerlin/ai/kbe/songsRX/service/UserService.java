﻿package de.htwBerlin.ai.kbe.songsRX.service;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.htwBerlin.ai.kbe.songsRX.bean.Song;
import de.htwBerlin.ai.kbe.songsRX.bean.SongList;
import de.htwBerlin.ai.kbe.songsRX.bean.User;
import de.htwBerlin.ai.kbe.songsRX.storage.ISongDAO;
import de.htwBerlin.ai.kbe.songsRX.storage.ISongListDAO;
import de.htwBerlin.ai.kbe.songsRX.storage.IUserDAO;
import de.htwBerlin.ai.kbe.songsRX.storage.TokenStorageInterface;


@Path("/userId")
public class UserService {
	
    private IUserDAO userDao;
    private ISongListDAO songListDao;
    private ISongDAO songDao;

    
    private TokenStorageInterface tokenStorage;
    

    @Inject
    public UserService(IUserDAO dao, ISongListDAO songListDao, ISongDAO songDao, TokenStorageInterface tokenStorage) {
        this.userDao = dao;
        this.songListDao = songListDao;
        this.tokenStorage = tokenStorage;
        this.songDao = songDao;
    }

	@GET
	@Path("/{userId}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public User getUser(@PathParam("userId") String userId) {
		User user = userDao.findUserByUserId(userId);
		return user;
	}

	@GET
	@Path("/{userId}/songLists")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Collection<SongList> getAllSongListsOfUser(@PathParam("userId") String userId, @HeaderParam("Authorization") String authToken) {
		String tokenUserId = tokenStorage.getUserIdFromToken(authToken);
		User user = userDao.findUserByUserId(userId);
		if(user == null) {
			return null;
		}
		Collection <SongList> songLists = null;
		if(!tokenUserId.equals(userId)) {
			songLists = songListDao.findAllPublicSongListsOfUser(user);
		} else {
			songLists = songListDao.findAllSongListsOfUser(user);
		}
		return songLists;
	}
	
	
	@GET
	@Path("/{userId}/songLists/{songListId}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getSongList(
			@PathParam("userId") String userId, 
			@PathParam("songListId") Integer songListId,
			@HeaderParam("Authorization") String authToken) {
		
		String tokenUserId = tokenStorage.getUserIdFromToken(authToken);
		SongList songList = null;
		if(tokenUserId == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		if(!tokenUserId.equals(userId)) {
			songList = songListDao.findPublicSongListById(songListId);
		} else {
			songList = songListDao.findSongListById(songListId);
		}
		if(songList == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		return Response.ok().entity(songList).build();
	}

	@POST
	@Path("/{userId}/songLists")
//	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response addNewSongList(
	        @PathParam("userId") String userId,
	        @HeaderParam("Authorization") String authToken,
            SongList songList) { //should get the Payload

        //get user
		String tokenUserId = null;
		try {
			tokenUserId = tokenStorage.getUserIdFromToken(authToken);
		} catch (NoSuchElementException e) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
        User user = userDao.findUserByUserId(userId);
   
        if(!tokenUserId.equals(userId)) {
        	return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        
        List<Song> songsInSongList = songList.getSongs(); //songs in Payload
        
        for(Song song : songsInSongList) {
        	if(songDao.findSongById(song.getId()) == null) {
                return Response.status(Response.Status.BAD_REQUEST).build();
        	}
        }

        songList.setUser(user);
        
        //anything is fine and songlist can be added
        int newSongListId = songListDao.saveSongList(songList);
		return Response.created(URI.create("/userId/" + user.getUserId() + "/songLists/" + Integer.toString(newSongListId))).entity("Created new Song List").build();
	}
	
	@DELETE
	@Path("/{userId}/songLists/{songListId}")
//	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deleteSongList(
			@PathParam("userId") String userId, 
			@PathParam("songListId") Integer songListId,
			@HeaderParam("Authorization") String authToken) {
		
		String tokenUserId = null;
        SongList songList = songListDao.findSongListById(songListId);
		try {
			tokenUserId = tokenStorage.getUserIdFromToken(authToken);
		} catch (NoSuchElementException e) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		if(songList == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
        if(!tokenUserId.equals(userId) || !songList.getUser().getUserId().equals(tokenUserId)) {
        	return Response.status(Response.Status.UNAUTHORIZED).build();
        }

		songListDao.deleteSongList(songListId);
		return Response.ok("Deleted songList").build();
	}
}