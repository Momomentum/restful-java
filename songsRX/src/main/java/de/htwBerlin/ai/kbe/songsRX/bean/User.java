package de.htwBerlin.ai.kbe.songsRX.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlInlineBinaryData;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="user")
@XmlRootElement(name = "user")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="user_id")
	private String userId;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="first_name")
	private String firstName;
	
	@OneToMany(mappedBy="user", targetEntity=SongList.class, fetch=FetchType.EAGER)
	private List<SongList> songLists;
	
	public User() {
		songLists = new ArrayList<SongList>();
	}
	
	
	@XmlElement
	public int getId() {
		return id;
	}

	@XmlElement
	public String getUserId() {
		return userId;
	}

	@XmlElement
	public String getLastName() {
		return lastName;
	}

	@XmlElement
	public String getFirstName() {
		return firstName;
	}
	
	@XmlElement
	public List<SongList> getSongLists() {
		return songLists;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void addSongList(SongList songList) {
		this.songLists.add(songList);
	}
	
	public void removeSongList(SongList songList) {
		this.songLists.remove(songList);
	}
}
