package de.htwBerlin.ai.kbe.songsRX.storage;

public interface TokenStorageInterface {
		
	public String updateUserToken(String userId);
	public Boolean authenticate(String token);
	public String getUserIdFromToken(String token);
}
