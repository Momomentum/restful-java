package de.htwBerlin.ai.kbe.songsRX.storage;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Random;


public class TokenStorage implements TokenStorageInterface{

	private static Map<String, String> storage = new HashMap<String,String>();

	
	public String updateUserToken(String userId) {
		String token = "";
		if(storage.containsKey(userId)) {
			token = storage.get(userId);
		} else {
			token = getToken(10);
			storage.put(userId, token);
		}
		return token;
	}
	
	
	private String getToken(int chars) {
	    String CharSet = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890";
	    String Token = "";
	    for (int a = 1; a <= chars; a++) {
	        Token += CharSet.charAt(new Random().nextInt(CharSet.length()));
	    }
	    return Token;
	}
	
	public Boolean authenticate(String token) {
		Boolean tokenExists = false;
		for(String tok : storage.values()) {
			//tokenExists = true;
			if(tok.equals(token)) {
				tokenExists = true;
			}
		}
		return tokenExists;
	}
	
	public String getUserIdFromToken(String token) {
		
		if(!storage.containsValue(token)) {
			throw new NoSuchElementException();
		}
		for(Entry<String, String> entry:storage.entrySet()) {
			if(entry.getValue().equals(token))
				return entry.getKey();
		}
		throw new NoSuchElementException();
	}
}
