package de.htwBerlin.ai.kbe.songsRX.storage;

import java.util.Collection;

import de.htwBerlin.ai.kbe.songsRX.bean.User;


public interface IUserDAO {
    /**
     * Retrieves a user
     * 
     * @param id
     * @return
     */
    public User findUserById(Integer id);

    /**
     * Retrieves all users
     * 
     * @return
     */
    public Collection<User> findAllUsers();

    /**
     * Save a new user
     * 
     * @param user
     * @return the Id of the new users
     */
    public Integer saveUser(User user);
    
    /**
     * Deletes the user for the provided id
     * @param id
     */
    public void deleteUser(Integer id);

	public User findUserByUserId(String id);
}
