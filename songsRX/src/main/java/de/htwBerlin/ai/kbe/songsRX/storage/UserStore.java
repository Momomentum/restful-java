package de.htwBerlin.ai.kbe.songsRX.storage;


import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


import de.htwBerlin.ai.kbe.songsRX.bean.User;


public class UserStore {
	
	private static Map<Integer,User> storage;
	private static UserStore instance = null;
	private static AtomicInteger highestId = null;

	private UserStore() {
		storage = new HashMap<Integer,User>();
	}
	
	public synchronized static UserStore getInstance() {
		if (instance == null) {
			instance = new UserStore();
		    UserStore userStore = UserStore.getInstance();
		    highestId = new AtomicInteger(0);
		    
		    // User user1 = new User("user1", "lastName1", "firstName1");
		    // User user2 = new User("user2", "lastName2", "firstName2");
		    // User user3 = new User("eschuler", "lastName3", "firstName3");
//		    User user1 = new User("me", "Mysel", "Me");
//		    User user2 = new User("eschuler", "Schuler", "Elena");
//		    
//		    userStore.addNewUser(user1);
//		    userStore.addNewUser(user2);
		}
		return instance;
	}

	public User getUser(Integer id) {
		return storage.get(id);
	}
	
	public User getUserByUserId(String userId) {
		User user = null;
		for (User us : storage.values()) {
		    if(us.getUserId().equals(userId)) {
		    	user = us;
		    }
		}
		return user;
	}
	
	public Collection<User> getAllUsers() {
		return storage.values();
	}
	
	public Integer addNewUser(User user) {
		user.setId(highestId.incrementAndGet());
		storage.put(user.getId(), user);
		return user.getId();	
	}
	
	
	public Integer addUser(User user) {
		storage.put(user.getId(), user);
		return user.getId();
	}
	
	public Integer updateUser(User user) {
		storage.put(user.getId(), user);
		return user.getId();	
	}
	
	// returns deleted user
	public Integer deleteUser(Integer id) {
		storage.remove(id);
		return id;
	}

}
