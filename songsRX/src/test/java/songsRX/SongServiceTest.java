package songsRX;

import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import de.htwBerlin.ai.kbe.songsRX.bean.Song;
import de.htwBerlin.ai.kbe.songsRX.service.AuthService;
import de.htwBerlin.ai.kbe.songsRX.service.SongService;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class SongServiceTest extends JerseyTest{
	
//	private String allSongsXml = "<songs>"
//			+ "<song><album>Ghostbusters</album><artist>Fall Out Boy, Missy Elliott</artist><id>64</id><released>2016</released><title>Ghostbusters (I'm not a fraid)</title></song>"
//			+ "<song><album>Thank You</album><artist>Meghan Trainor, Kelli Trainor</artist><id>82</id><released>2016</released><title>Mom</title></song>"
//			+ "<song><album>Top Hits 2017</album><artist>Gnash</artist><id>37</id><released>2017</released><title>i hate u, i love u</title></song>"
//			+ "<song><album>Bloom</album><artist>Camila Cabello, Machine Gun Kelly</artist><id>55</id><released>2017</released><title>Bad Things</title></song>"
//			+ "<song><album>Glory</album><artist>Britney Spears</artist><id>9</id><released>2016</released><title>Private Show</title></song>"
//			+ "<song><artist>Iggy Azalea</artist><id>73</id><released>2016</released><title>Team</title></song>"
//			+ "<song><album>Lukas Graham (Blue Album)</album><artist>Lukas Graham</artist><id>10</id><released>2015</released><title>7 Years</title></song>"
//			+ "<song><album>Trolls</album><artist>Justin Timberlake</artist><id>91</id><released>2016</released><title>Can’t Stop the Feeling</title></song>"
//			+ "<song><album>Thank You</album><artist>Meghan Trainor</artist><id>28</id><released>2016</released><title>No</title></song>"
//			+ "<song><album>At Night, Alone.</album><artist>Mike Posner</artist><id>46</id><released>2016</released><title>I Took a Pill in Ibiza</title></song>"
//			+ "</songs>";
//	
//	private String allSongsJson = "[{"
//			  + "\"id\" : 10,"
//			  + "\"title\" : \"7 Years\","
//			  + "\"artist\" : \"Lukas Graham\","
//			  + "\"album\" : \"Lukas Graham (Blue Album)\","
//			  + "\"released\" : 2015"
//			+ "}, {"
//			  + "\"id\" : 9,"
//			  + "\"title\" : \"Private Show\","
//			  + "\"artist\" : \"Britney Spears\","
//			  + "\"album\" : \"Glory\","
//			  + "\"released\" : 2016"
//			+ "}, {"
//			  + "\"id\" : 28,"
//			  + "\"title\" : \"No\","
//			  + "\"artist\" : \"Meghan Trainor\","
//			  + "\"album\" : \"Thank You\","
//			  + "\"released\" : 2016"
//			+ "}, {"
//			  + "\"id\" : 37,"
//			  + "\"title\" : \"i hate u, i love u\","
//			  + "\"artist\" : \"Gnash\","
//			  + "\"album\" : \"Top Hits 2017\","
//			  + "\"released\" : 2017"
//			+ "}, {"
//			  + "\"id\" : 46,"
//			  + "\"title\" : \"I Took a Pill in Ibiza\","
//			  + "\"artist\" : \"Mike Posner\","
//			  + "\"album\" : \"At Night, Alone.\","
//			  + "\"released\" : 2016"
//			+ "}, {"
//			  + "\"id\" : 55,"
//			  + "\"title\" : \"Bad Things\","
//			  + "\"artist\" : \"Camila Cabello, Machine Gun Kelly\","
//			  + "\"album\" : \"Bloom\","
//			  + "\"released\" : 2017"
//			+ "}, {"
//			  + "\"id\" : 64,"
//			  + "\"title\" : \"Ghostbusters (I'm not a fraid)\","
//			  + "\"artist\" : \"Fall Out Boy, Missy Elliott\","
//			  + "\"album\" : \"Ghostbusters\","
//			  + "\"released\" : 2016"
//			+ "}, {"
//			  + "\"id\" : 73,"
//			  + "\"title\" : \"Team\","
//			  + "\"artist\" : \"Iggy Azalea\","
//			  + "\"album\" : null,"
//			  + "\"released\" : 2016"
//			+ "}, {"
//			  + "\"id\" : 82,"
//			  + "\"title\" : \"Mom\","
//			  + "\"artist\" : \"Meghan Trainor, Kelli Trainor\","
//			  + "\"album\" : \"Thank You\","
//			  + "\"released\" : 2016"
//			+ "}, {"
//			  + "\"id\" : 91,"
//			  + "\"title\" : \"Can’t Stop the Feeling\","
//			  + "\"artist\" : \"Justin Timberlake\","
//			  + "\"album\" : \"Trolls\","
//			  + "\"released\" : 2016"
//			+ "} ]";
//	
	  	@Override
	    protected Application configure() {
	        return new ResourceConfig(SongService.class);
	    }
	
//	  	@Test
//	    public void getSongsWithInvalidAcceptHeaderShouldReturn406(){
//	        Response response = target("/songs").request(MediaType.APPLICATION_SVG_XML_TYPE).get();
//	        Assert.assertEquals(406, response.getStatus());
//	  	}
//	  	
//	  	@Test
//	    public void getSongsShouldReturnAllSongsJSON() throws JSONException {
//	        String response = target("/songs").request(MediaType.APPLICATION_JSON).get(String.class);
//	        JSONAssert.assertEquals(allSongsJson, response, false);
//	    }
//	  	
//	  
//	  	@Test
//	    public void getSongsShouldReturnAllSongsXML() throws SAXException, IOException {
//	        String response = target("/songs").request(MediaType.APPLICATION_XML).get(String.class);
//	        XMLAssert.assertXMLEqual(allSongsXml, response);
//	  	}
//	    
//	    @Test
//	    public void getSongDefaultContentTypeShouldBeXML() {
//	        String response = target("/songs/10").request().get(String.class);
//	        System.out.println(response);
//	        Assert.assertTrue(response.startsWith("<?xml"));
//	    }
//	    
//	    @Test
//	    public void getSongWithInvalidIdShouldReturn404() {
//	        Response response = target("/songs/blub").request().get();
//	        Assert.assertEquals(404, response.getStatus());
//	    }
//	    
//	    @Test
//	    public void getSongWithNonExistingIdShouldReturn404() {
//	        Response response = target("/songs/22").request().get();
//	        Assert.assertEquals(404, response.getStatus());
//	    }
//	    
//	    @Test
//	    public void getSongWithAcceptJsonHeaderShouldReturnJsonContent() throws JSONException {
//	        String response = target("/songs/10").request(MediaType.APPLICATION_JSON).get(String.class);
//	        System.out.println(response);
//	        String expect = "{"
//	        		  + "\"id\" : 10,"
//	        		  + "\"title\" : \"7 Years\","
//	        		  + "\"artist\" : \"Lukas Graham\","
//	        		  + "\"album\" : \"Lukas Graham (Blue Album)\","
//	        		  + "\"released\" : 2015"
//	        		+ "}";
//	        JSONAssert.assertEquals(expect, response, false);
//	    }
//	    
//	    @Test
//	    public void getSongWithValidIdShouldReturnSong() {
//	        Song song = target("/songs/10").request(MediaType.APPLICATION_JSON).get(Song.class);
//	        System.out.println(song);
//	        Assert.assertEquals(10, song.getId().intValue());
//	    }
//	    
//	    @Test
//	    public void createSongWithXMLBodyShouldReturn201AndID() {
//	    	Song song = new Song(1, "TestTitle", "TestArtist", "TestAlbum", 1995);
//	        Response response = target("/songs").request().post(Entity.xml(song));
//	        Assert.assertEquals(201, response.getStatus());
//	        
//	        target("/songs/" + response.readEntity(String.class)).request().delete();
//	    }
//	    
//	    @Test
//	    public void createSongWithJsonBodyShouldReturn201AndID() {
//	    	Song song = new Song(1, "TestTitle", "TestArtist", "TestAlbum", 1995);
//	        Response response = target("/songs").request().post(Entity.json(song));
//	        Assert.assertEquals(201, response.getStatus());
//	        
//	        target("/songs/" + response.readEntity(String.class)).request().delete();
//	    }
//	    
////	    @Test
////	    public void createSongWithInvalidContentTypeShouldReturn415() {
////	    	Song song = new Song(1, "TestTitle", "TestArtist", "TestAlbum", 1995);
////	        Response response = target("/songs").request().post(Entity.text(song));
////	        Assert.assertEquals(415, response.getStatus());
////	    }
//	    
//	    @Test
//	    public void deleteSongShouldDeleteSongAndReturn202() {
//	    	Song song = new Song(1, "TestTitle", "TestArtist", "TestAlbum", 1995);
//	        Response res = target("/songs").request().post(Entity.xml(song));
//	        
//	    	Response response = target("/songs/" + res.readEntity(String.class)).request().delete();
//	    	Assert.assertEquals(202, response.getStatus());
//	    }
//	    
//	    
//	    @Test
//	    public void deleteSongWithInvalidIdShouldReturn404() {
//	    	Response response = target("/songs/1").request().delete();
//	    	Assert.assertEquals(404, response.getStatus());
//	    }
//	    
//	    @Test
//	    public void deleteSongWithStringIdShouldReturn404() {
//	    	Response response = target("/songs/bla").request().delete();
//	    	Assert.assertEquals(404, response.getStatus());
//	    }
//	    
//	    @Test
//	    public void updateSongShouldUpdateSongAndReturn204() throws JSONException {
//	    	Song song = new Song(1, "TestTitle", "TestArtist", "TestAlbum", 1995);
//	        Response res = target("/songs").request().post(Entity.xml(song));
//	        int id = Integer.parseInt(res.readEntity(String.class));
//	        
//	    	Song updateSong = new Song(id, "NewTitle", "NewArtist", "NewAlbum", 1996);
//	    	Response response = target("/songs/" + id).request().put(Entity.xml(updateSong));
//	    	Assert.assertEquals(204, response.getStatus());
//	    	
//	    	String expect = "{"
//	        		  + "\"id\" :" + id + ","
//	        		  + "\"title\" : \"NewTitle\","
//	        		  + "\"artist\" : \"NewArtist\","
//	        		  + "\"album\" : \"NewAlbum\","
//	        		  + "\"released\" : 1996"
//	        		+ "}";
//	    	
//	    	String jsonResponse = target("/songs/" + id).request(MediaType.APPLICATION_JSON).get(String.class);
//	    	System.out.println(jsonResponse);
//	    	JSONAssert.assertEquals(expect, jsonResponse, false);
//	        
//	        target("/songs/" + id).request().delete();
//	    }
//
//	    @Test
//	    public void updateSongWithInvalidIdShouldReturn404() {
//	    	Song updateSong = new Song(1, "NewTitle", "NewArtist", "NewAlbum", 1996);
//	    	Response response = target("/songs/1").request().put(Entity.xml(updateSong));
//	    	Assert.assertEquals(404, response.getStatus());
//	    }
//	    
//	    @Test
//	    public void updateSongWithStringIdShouldReturn404() {
//	    	Song updateSong = new Song(1, "NewTitle", "NewArtist", "NewAlbum", 1996);
//	    	Response response = target("/songs/blalub").request().put(Entity.xml(updateSong));
//	    	Assert.assertEquals(404, response.getStatus());
//	    }
}
