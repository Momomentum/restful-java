package songsRX;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;

import de.htwBerlin.ai.kbe.songsRX.di.DependencyBinder;
import de.htwBerlin.ai.kbe.songsRX.filter.AuthenticationFilter;
import de.htwBerlin.ai.kbe.songsRX.service.AuthService;


public class AuthServiceTest extends JerseyTest{
	
	
  	@Override
    protected Application configure() {
    	ResourceConfig conf = new ResourceConfig(AuthService.class);
//    	conf.register(AuthService.class);
    	//conf.register(new AuthenticationFilter());
    	conf.register(new DependencyBinder());
    	return conf;
//    	{
//    		{
//    			register(new DependencyBinder());
//    			register(new AuthenticationFilter());
//    			register(AuthService.class);
//    		}
//    	};
    }


//	@Test
//	public void createTokenShouldReturn200() {
//		
//		Response response = target("/auth?userId=user1").request(MediaType.TEXT_PLAIN).get();
//        Assert.assertEquals(200, response.getStatus());
//	}
/*		
	@Test
	public void getAllSongsWithoutAuthShouldReturn401() {
		
	}
	
	@Test
	public void getAllSongsWithCorrectAuthShouldReturn200() {
		
	}
*/

}
