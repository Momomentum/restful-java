package de.htwBerlin.ai.kbe.myFirstServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
name = "HeaderServlet",
urlPatterns = "/getHeaders",
initParams = {
		@WebInitParam(name = "signature", 
		    value = "Thanks for using AI-KBE's Echo Servlet © 2017! ")
}
)
public class HeaderServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected String mySignature = null;

	public void init(ServletConfig servletConfig) throws ServletException{
	    this.mySignature = servletConfig.getInitParameter("signature");
    }
	  
	@Override
	public void doGet(HttpServletRequest request, 
	
			HttpServletResponse response) throws IOException{
	
	   // alle Parameter (keys) 
	   Enumeration<String> paramNames = request.getParameterNames();
	   // request.getHeaderNames();
	   
	   String responseStr = "";
	   String param = "";
//	   HashMap<String, String> headerMap = new HashMap<String, String>();
	   
	   String test = "Empty";
	   boolean fetchAll = false;
	   boolean foundHeader = false;
	   while (paramNames.hasMoreElements() && !foundHeader) {
		   param = paramNames.nextElement();
//		   test = param;
		   if(param.equals("all")) {
			   fetchAll = true;
			   foundHeader = true;
			   test = "In all";
		   } else if(param.equals("header")) {
			   String header = request.getHeader(request.getParameter(param));
			   if(header != null) {
//				   headerMap.put(param, header);
				   responseStr = responseStr + param + "=" + header +"\n";
				   foundHeader = true;
			   }
		   }
	   }
	   
	   if(fetchAll) {
		   responseStr = "";
		   Enumeration<String> requestHeaders = request.getHeaderNames();
		   while (requestHeaders.hasMoreElements()) {
			   param = requestHeaders.nextElement();
			   String header = request.getHeader(param);
//			   headerMap.put(param, header);
			   responseStr = responseStr + param + "=" + header +"\n";
		   }
	   }
	   
		try (PrintWriter out = response.getWriter()) {
			responseStr += "\n" + mySignature;
			out.println(responseStr);
//			out.println(request.getHeader("accept"));
		}
	}
}
