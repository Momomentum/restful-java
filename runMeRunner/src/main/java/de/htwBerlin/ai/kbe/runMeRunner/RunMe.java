package de.htwBerlin.ai.kbe.runMeRunner;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//a)  
//Definieren Sie bitte eine Annotation namens „RunMe“. Diese soll zur Laufzeit ausgewertet 
//werden und soll nur für Methoden verwendet werden. Die Annotation soll einen Parameter names 
//input, ohne default-Wert, definieren: String input 
		
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface RunMe {
	String input();
}
