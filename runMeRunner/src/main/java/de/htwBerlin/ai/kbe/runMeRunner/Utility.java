package de.htwBerlin.ai.kbe.runMeRunner;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @Author Chris Heiden, Moritz Mueller-Guthof
 * Class that holds methods to configure runtime environment by user defined config file.
 */
public class Utility {
	
	/** Fetch Proerties from a config file.
     *  The config file shoould be possible to be open with read permissions and contain valid java properties.
     *  @param  filepath    The path to the file containing the java properties.
     *  @return A map of the properties found in config file 
     */
	public Map<String,String> fetchConfigProperties(String filepath) throws IOException, MalformedConfigException{		
		
		File propertiesFile = new File(filepath);
		Properties props = new Properties();
		
		Map<String, String> map = new HashMap<String, String>();
		 
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(propertiesFile)); // Read file (buffered)
		props.load(bis); // Load the properties
		Enumeration<?> e = props.propertyNames(); // <?> is a shorthand for <? extends Object>, it's also known as an unbounded wildcard. So you can specify any type of object in your generic.
		// Iterate over all elements and extract keys and values
		while(e.hasMoreElements()) {
			String key = (String)e.nextElement();
			String value = props.getProperty(key);
			
			if(value.equals("") || value.equals(null)) {
				throw new MalformedConfigException();
			}
			map.put(key, value);
			//System.out.println(key + "," + value);		
		}
		return map;
	}
}


