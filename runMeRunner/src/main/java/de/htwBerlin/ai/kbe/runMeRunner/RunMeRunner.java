package de.htwBerlin.ai.kbe.runMeRunner;

import de.htwBerlin.ai.kbe.uebung1.MyEmailSyntaxChecker;

//b) Implementieren Sie bitte eine Klasse, die mehrere Methoden implementiert. Einige dieser Methoden sollen 
//mit der @RunMe -Annotation versehen werden, die anderen Methoden sollen keine oder andere Annotationen 
//besitzen. Eine  Methode dieser Klasse muss so aussehen:
//wobei YOUREmailSyntaxChecker die EmailChecker-Klasse aus 
//Ihrer EmailChecker-Bibliothek (Aufgabe 1) ist.

/**
 * @Author Chris Heiden, Moritz Mueller-Guthof
 * Simple class to test RunMe Annotation
 * 
 */
public class RunMeRunner {
	
	@RunMe(input = "myName@domain.com")
	public boolean validateEmailOne(String input) {
		MyEmailSyntaxChecker esc = new MyEmailSyntaxChecker();
		return esc.isValidEmailAddress(input);
	}
	
	@RunMe(input = "myName@..com")
	public boolean validateEmailTwo(String input) {
		MyEmailSyntaxChecker esc = new MyEmailSyntaxChecker();
		return esc.isValidEmailAddress(input);	
	}
	
	@Deprecated
	public void methodThree(String input) {
		System.out.println ("in methodThree");
	}
	
	public void methodFour(String input) {
		System.out.println ("In methodFour");
	}
	
	public void methodFive(String input) {
		System.out.println ("In methodFive");
	}

}
