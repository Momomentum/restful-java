package de.htwBerlin.ai.kbe.runMeRunner;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * @Author Moritz Mueller-Guthof, Chris Heiden
 *
 * Main Class. Catches all thrown exceptions here.
 * In order to use this class call with commandline arguments like so:
 * java App.class [argument]
 * Replace [argument] with a path to a config file that holds information about which classes
 * RunMe-annotated methods should be run
 * 
 */
public class App 
{
    public static void main( String[] args )
    {
    	
//		RunMeRunner test = new RunMeRunner();
//		test.methodFive("hallo");
//		test.methodFour("ich");
//		test.methodThree("bin's");
//		System.out.println(test.validateEmailTwo("myName@domain.com"));
//		System.out.println(test.validateEmailOne("myName@..com"));
/*
		Utility test = new Utility();
		try {
			test.fetchConfigProperties("./src/test/java/wrongProp.config");
		} catch (Exception e) {
			System.err.println("Something went wrong!");
}*/

    	try {
    		if(args.length == 1) {
    			MethodRunner run = new MethodRunner(args[0]);
    		}
    		else {
    			System.out.println("Please specify property file as argument.");
    		}
    	}
    	catch (IOException e) {
    		System.out.println("File does not exist");
    	}
    	catch (EmptyConfigException e) {
    		System.out.println("Config File is empty. Cannot be processed!");
    	} 
    	catch (MalformedConfigException e) {
    		System.out.println("Config File has wrong format!");
    	} 
    	catch (ClassNotFoundException e) {
    		System.out.println("Class does not exist and cannot be instatiated!");
    	} 
    	catch (IllegalArgumentException e) {
    		System.out.println("Wrong argument or wrong argument count!");
    	}
    	catch (Exception e) {
    		System.out.println("Something went wrong. Please contact the maintainers!");
		}	
    }
}
