package de.htwBerlin.ai.kbe.runMeRunner;

/**
 * @Author Chris Heiden, Moritz Mueller Guthof
 * Exception that can be thrown if an empty config file is specified, but config properties are
 * needed.
 * 
 */
public class EmptyConfigException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmptyConfigException() {
		
	}
	
	public EmptyConfigException(String message) {
		super(message);
	}
	
	
	
}
