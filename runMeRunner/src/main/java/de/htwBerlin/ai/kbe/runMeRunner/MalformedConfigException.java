package de.htwBerlin.ai.kbe.runMeRunner;

public class MalformedConfigException extends Exception{

	/**
	 * @Author Chris Heiden, Moritz Mueller-Guthof
     * Exception that can be thrown when the config file doesnt fit the desired scheme
	 */
	private static final long serialVersionUID = 1L;

	public MalformedConfigException() {
		
	}
	
	public MalformedConfigException(String message) {
		super(message);
	}
}
