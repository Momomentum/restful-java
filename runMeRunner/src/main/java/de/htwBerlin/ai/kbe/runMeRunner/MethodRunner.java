package de.htwBerlin.ai.kbe.runMeRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import java.io.IOException;

/*
	Bitte achten Sie darauf, dass Nutzer oft unkonzentriert sind:
		- vergessen, ein configFile anzugeben
		- geben ein configFile an, welches nicht existiert
		- geben ein configFile an, welches leer ist oder kein Properties-File ist,
		- die erwartete Property ist nicht im configFile enthalten.
	Ihr Programm sollte Nutzer-Fehler gut handhaben können.
*/

/**
 * @Author Chris Heiden, Moritz Mueller-Guthof
 * Class that can run methods that are RunMe-Annotated. The Object on which the method is invoked is created on runtime
 * and is defined by a config file which path must be passed to the constructor.
 * 
 */
public class MethodRunner {
    /** Constructor calls all RunMe-annotated methods.
     *  @param  pathToFile  Path to a valid properties file that contains the desired classname. 
     */
	public MethodRunner(String pathToFile)
    throws IOException, EmptyConfigException, MalformedConfigException, ClassNotFoundException, 
        InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
		
		Utility callRunMeConfig = new Utility(); // Create Utility to read properties file 
				
		HashMap<String, String> runProp = (HashMap<String, String>) callRunMeConfig.fetchConfigProperties(pathToFile); // fetch properties from Config file
		if(runProp.size() == 0) {
			throw new EmptyConfigException(); // If there are no properties in config throw an empty config exception.
		}
		
		String className = runProp.get("classWithRunMeAnnos"); // Get the value of property
		
		if(className == null || className.equals("")){
			throw new MalformedConfigException(); // If value is not defined throw a Malformed Config Exception.
		}
	    
		Class clazz = Class.forName(className); // Load class from config file name
		Object obj = clazz.newInstance(); // Create new Class object
	    Method methods[] =  clazz.getDeclaredMethods(); // Fetch all class methods
	    Set<Method> annotatedMethods = new HashSet<Method>(); 
	    
        // For every class method check if RunMe Annotation exists and invoke that method with annotated input on object
	    for (final Method method : methods) {
	    	RunMe runMe = method.getAnnotation(RunMe.class);
	    	if(runMe != null) {
        	  method.invoke(obj, runMe.input());
        	  annotatedMethods.add(method);
	    	}
        }
	}
}
