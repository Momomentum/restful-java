package de.htwBerlin.ai.kbe.runMeRunner;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class UtilityTest {
	
	//Testcases
	//data doesn't exist -> Failure (x)
	//wrong file format -> Failure (x)
	//correct file format -> Correct (x)


	//File is not named
	@Test(expected = NullPointerException.class)
	public void notNamedFileShouldThrowException() throws Exception {
		Utility util = new Utility();
		util.fetchConfigProperties(null);
	}
	
	//File does not exist 
	@Test(expected = IOException.class)
	public void WrongFileNameShouldThrowException() throws Exception {
		Utility util = new Utility();
		util.fetchConfigProperties("pro.config");
	}
	
	//File is correct named
/*	@Test()
	public void correctFileNameShouldNotThrowException() throws Exception{
		Utility util = new Utility();
		util.fetchConfigProperties("properties.config");
	}
*/

	@Test()
	public void validPropertyFileIsCorrectlyRead() throws Exception{
		Utility util = new Utility();
		Map<String,String> ckeckMap = util.fetchConfigProperties("properties.config");
		Map<String, String> validMap = new HashMap<String, String>();
		validMap.put("name","chris");
		validMap.put("alter","22");
		validMap.put("ort","Berlin");
		
		assertEquals(ckeckMap,validMap);

	}

	//File is correct named but the syntax is wrong
	@Test(expected = Exception.class)
	public void wrongPropertySyntaxShouldThrowException() throws Exception {
		Utility util = new Utility();
		util.fetchConfigProperties("wrongProp.config");
	}
}
