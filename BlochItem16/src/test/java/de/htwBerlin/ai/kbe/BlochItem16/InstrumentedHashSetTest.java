package de.htwBerlin.ai.kbe.BlochItem16;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Test;

public class InstrumentedHashSetTest {
	

	@Test
	public void addNumberOfElementsShouldIncreaseElementCountByTheSameNumber() {
		InstrumentedHashSet<String> s = new InstrumentedHashSet<String>(new HashSet<String>());
		s.addAll(Arrays.asList("Snap", "Crackle", "Pop"));
		assertEquals("Number of Elements add does nnot fit.", 3 ,s.getAddCount());
	}
	

}
