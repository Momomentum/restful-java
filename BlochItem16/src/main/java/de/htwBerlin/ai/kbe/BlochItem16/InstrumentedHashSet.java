package de.htwBerlin.ai.kbe.BlochItem16;

import java.util.*;

/**
 * Doesn't work! Demonstrates the danger in inheriting from a concrete class.
 */


/**
 * Aufgabe 2: Composition over Inheritance
 * Bitte schauen Sie sich folgenden Code an:
 * a) Was ist die Ausgabe? 6
 * 
 * b) Warum? addAll() wird aufgerufen und dabei wird die Variable addCount auf drei gesetzt, zudem 
 * 			 wird in der Vaterklasse auch nochmal die addAll() aufgerufen, die wiederum die 
 * 			 überschriebene add()-Memberfunktion aufruft und damit die wieder die variable addcount 
 * 			 um drei erhöht.
 * 
 * c) Wie sieht eine bessere Lösung aus? Bitte implementieren und testen Sie diese. Oke!
 * d) Bitte checken Sie Ihren Code ins git ein: kbeWS17TEAMNAME/BlochItem16
 */

//@SuppressWarnings("serial")
//public class InstrumentedHashSet<E> extends HashSet<E> {
//	
//	
//
//	// The number of attempted element insertions
//	private int addCount = 0;
//
//	public InstrumentedHashSet() {
//
//	}
//
//	public InstrumentedHashSet(int initCap, float loadFactor) {
//		super(initCap, loadFactor);
//	}
///*
//	@Override
//	public boolean add(E e) {
//		addCount++;
//		return super.add(e);
//	}*/
//
//	@Override
//	public boolean addAll(Collection<? extends E> c) {
//		addCount += c.size();
//		return super.addAll(c);
//	}
//
//	public int getAddCount() {
//		return addCount;
//	}
//
//	public static void main(String[] args) {
//		InstrumentedHashSet<String> s = new InstrumentedHashSet<String>();
//		s.addAll(Arrays.asList("Snap", "Crackle", "Pop"));
//		System.out.println(s.getAddCount());
//	}
//}

public class InstrumentedHashSet<E> implements Set<E> {

	
	 private final Set<E> wrappedSet;

	  public int addCount = 0;

	  public InstrumentedHashSet(Set<E> wrappedSet) {
	    this.wrappedSet = wrappedSet;
	  }

	  public boolean add(E e) {
	    addCount += 1;
	    return wrappedSet.add(e);
	  }

	  public boolean addAll(Collection<? extends E> c) {
	    addCount += c.size();
	    return wrappedSet.addAll(c);
	  }
	
	  public int getAddCount() {
			return addCount;
		}
	
	
	
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}


	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	public void clear() {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
