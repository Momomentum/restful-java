package de.htwBerlin.ai.kbe.uebung1;

import static org.junit.Assert.fail;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.junit.Test;


public class ExternalEmailSyntaxCheckerTest {
	

	@Test(expected = Exception.class)
	public void nullEmailShouldThrowException() throws AddressException {
		InternetAddress tester = new InternetAddress(null);
		tester.validate();
	}
	
	@Test(expected = Exception.class)
	public void emptyEmailShouldThrowException() throws AddressException {
		InternetAddress tester = new InternetAddress("");
		tester.validate();
	}
	
	@Test(expected = Exception.class)
	public void noAtSignShouldThrowException() throws AddressException {
		InternetAddress tester = new InternetAddress("christophergooglemail.com");
		tester.validate();
	}
	
	@Test(expected = Exception.class)
	public void noDomainShouldThrowException() throws AddressException {
		InternetAddress tester = new InternetAddress("christopher@. .com");
		tester.validate();

	}
	
	@Test(expected = Exception.class)
	public void noUsernameShouldThrowException() throws Exception {
		InternetAddress tester = new InternetAddress("@googlemail..com");
		tester.validate();

	}
	
	@Test(expected = Exception.class)
	public void noTopLevelDomainShouldThrowException() throws Exception {
		InternetAddress tester = new InternetAddress("christopher@googlemail..");
		tester.validate();
	}
	
	@Test
	public void validEmailShouldNotThrowException() {
		InternetAddress tester;
		try {
			tester = new InternetAddress("christopher@googlemail.com");
			tester.validate();
		} catch (AddressException e1) {
			// TODO Auto-generated catch block
			fail();
		}
	}

}
