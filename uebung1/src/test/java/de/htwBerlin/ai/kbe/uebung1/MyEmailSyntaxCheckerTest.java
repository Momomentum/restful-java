package de.htwBerlin.ai.kbe.uebung1;

import static org.junit.Assert.*;

import org.junit.Test;

import de.htwBerlin.ai.kbe.uebung1.MyEmailSyntaxChecker;


public class MyEmailSyntaxCheckerTest {
	
	@Test
	public void nullEmailShouldReturnFalse() {
		MyEmailSyntaxChecker tester = new MyEmailSyntaxChecker();
		assertEquals("Null email should return false",false, tester.isValidEmailAddress(null));
	}
	
	@Test
	public void emptyEmailShouldReturnFalse() {
		MyEmailSyntaxChecker tester = new MyEmailSyntaxChecker();
		assertEquals("Null email should return false",false, tester.isValidEmailAddress(""));
	}
	
	@Test
	public void noAtSignShouldReturnFalse() {
		MyEmailSyntaxChecker tester = new MyEmailSyntaxChecker();
		assertEquals("noAtSignShouldReturnFalse",false, tester.isValidEmailAddress("christophergooglemail.com"));
	}
	
	@Test
	public void noDomainShouldReturnFalse() {
		MyEmailSyntaxChecker tester = new MyEmailSyntaxChecker();
		assertEquals("noDomainShouldReturnFalse",false, tester.isValidEmailAddress("christopher@.com"));
	}
	
	@Test
	public void noUsernameShouldReturnFalse() {
		MyEmailSyntaxChecker tester = new MyEmailSyntaxChecker();
		assertEquals("noUsernameShouldReturnFalse",false, tester.isValidEmailAddress("@googlemail.com"));
	}
	
	@Test
	public void noTopLevelDomainShouldReturnFalse() {
		MyEmailSyntaxChecker tester = new MyEmailSyntaxChecker();
		assertEquals("noTopLevelDomainShouldReturnFalse",false, tester.isValidEmailAddress("christopher@googlemail"));
	}
	
	@Test
	public void validEmailShouldReturnTrue() {
		MyEmailSyntaxChecker tester = new MyEmailSyntaxChecker();
		assertEquals("valid Email Should Return True",true, tester.isValidEmailAddress("christopher@googlemail.com"));
	}
	

}
