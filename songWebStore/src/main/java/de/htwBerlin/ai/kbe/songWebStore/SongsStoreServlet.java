package de.htwBerlin.ai.kbe.songWebStore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;

public class SongsStoreServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private static final String APPLICATION_JSON = "application/json";
	
	private static final String TEXT_PLAIN = "text/plain";
	
    private String songFilename = null;
	
    protected Map<Integer, Song> songStore = null;
    
    private Integer currentID = null;
    
    private String signature = null;
    
    private ObjectMapper mapper = null;
    
    private URL fileUrl = null;
    
    private OutputStream output;
    

    
	// load songStore from JSON file and set currentID
	public void init(ServletConfig servletConfig) throws ServletException {
	    // Read a JSON file and create song list:		
		this.signature = servletConfig.getInitParameter("signature");
	    mapper = new ObjectMapper();
	    InputStream input = this.getClass().getClassLoader().getResourceAsStream("songs.json");
	    fileUrl = this.getClass().getClassLoader().getResource("songs.json");
	    songStore = new ConcurrentHashMap<Integer, Song>();

		List<Song> songList;
		try {
			songList = (List<Song>) mapper.readValue(input, new TypeReference<List<Song>>(){});
			for (Song song : songList) {
				songStore.put(song.getId(), song);
			}
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}

	// if current URL is wrong -> Write down: A correct URL request should look like that ...    
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException { 		
		String param = null;
		String responseText = "";
		int status = 200;
		Enumeration<String> paramNames = request.getParameterNames();
		int size = 0;
		while (paramNames.hasMoreElements()) {
			param = paramNames.nextElement();
			size++;
		}
		if(size == 1) {

			if(param.equals("all"))
			{
				responseText = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(songStore.values());
			}
			else if(param.equals("songId")) 
			{
				Song song = null;
				try {
					song = songStore.get(Integer.parseInt(request.getParameter(param)));
					if(song == null) {
						status = 404;
						responseText = "{\"message\": \"Could not find Song with Id: " + request.getParameter("songId") + "\"}";
					} else {
						responseText = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(song);
					}
				} catch (NumberFormatException e) {
					status = 401;
					responseText = "{\"message\": \"Parameter value needs to be of type int!\"}";
				}
				
			}
			else 
			{
				responseText = "{\"message\": \"Parameter should be 'all' or 'songId'!\"}";
				status = 401;
			}
		} 
		else	
		{
			responseText = "{\"message\": \"Please specify 1 Get Parameter!\"}";
			status = 401;
		}
   		response.setContentType("application/json; charset=utf-8");
		response.setStatus(status);
		

		
		try (PrintWriter out = response.getWriter()) {
			out.println(responseText);
		}
	}
	
	private List<String> getmissingFields(Song song) {
		List<String> missingFieldsList = new ArrayList<String>();
		if(song.getAlbum() == null) {
			missingFieldsList.add("album");
		}
		if(song.getArtist() == null) {
			missingFieldsList.add("artist");
		}
		if(song.getReleased() == null) {
			missingFieldsList.add("released");
		}
		if(song.getTitle() == null) {
			missingFieldsList.add("title");	
		}
		return missingFieldsList;
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int status = 200;
		String responseText = "";
		InputStream input = request.getInputStream();
		Song song;
		try {
			song = mapper.readValue(input, new TypeReference<Song>(){});
		} catch (MismatchedInputException e) {
			response.setStatus(401);
			try (PrintWriter out = response.getWriter()) {
				out.println("{\"message\": \"Please send valid format!\"}");
			}
			return;
		}
		
		
		List<String> missingFields = this.getmissingFields(song);
		if(missingFields.isEmpty()) {
			song.setIdFromCounter();
			songStore.put(song.getId(), song);					
//			try {
//				File file = new File(fileUrl.toURI());
//				output = new FileOutputStream(file);
//				mapper.writeValue(output, songStore.values());
//				output.close();
//			} catch (URISyntaxException e) {
//				e.printStackTrace();
//			}
			responseText = "{\"message\": \"Added new Song " + song.getTitle() + ", with id: " + song.getId() + "\"}";

		} else {
			status = 401;
			responseText = "{\"message\": \"Could not create Song\", \"missingFields\": [";
			
			String errors = "";
			for (String field:missingFields) {
				errors += "\"" + field + "\", ";
			}
			errors = errors.substring(0, errors.length() - 2);
			responseText += errors + "]}";
		}
   		response.setContentType("application/json; charset=utf-8");
   		response.setStatus(status);
		try (PrintWriter out = response.getWriter()) {
			out.println(responseText);
		}
	}

	@Override
	public void destroy() {
		try {
			File file = new File(fileUrl.toURI());
			output = new FileOutputStream(file);
			mapper.writeValue(output, songStore.values());
			output.close();
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}
	}
}
