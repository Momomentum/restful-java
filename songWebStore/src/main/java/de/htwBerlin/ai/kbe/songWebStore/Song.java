package de.htwBerlin.ai.kbe.songWebStore;

import java.util.concurrent.atomic.AtomicInteger;

public class Song {

	private Integer id;
	private String title;
	private String artist;
	private String album;
	private Integer released;
	
	private static AtomicInteger idCounter = new AtomicInteger(1);
	
	
	public Song () {	}
	
	
	public Song (Integer id, String title, String artist, String album, Integer released) {
		this.id = id;
		this.title = title;
		this.artist = artist;
		this.album = album;
		this.released = released;
	}
		
	
	public void setIdFromCounter() {
				
		setId(Song.idCounter.get());
	}
	
	private void incrementCounter() {
		idCounter.incrementAndGet();
	}
	
	public Integer getId() {
		return id;
	}
	
	void setId(Integer id) {
		this.id = id;
		this.incrementCounter();
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getArtist() {
		return artist;
	}
	
	public String getAlbum() {
		return album;
	}
	
	public Integer getReleased() {
		return released;
	}
}
