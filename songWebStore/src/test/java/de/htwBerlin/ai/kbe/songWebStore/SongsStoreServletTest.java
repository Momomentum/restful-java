package de.htwBerlin.ai.kbe.songWebStore;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SongsStoreServletTest {
	
    private SongsStoreServlet servlet;
    private ServletConfig config;
//    private HttpServletRequest request;
//    private HttpServletResponse response;
    MockHttpServletRequest request;
    MockHttpServletResponse response;
    private ObjectMapper objectMapper;
    
    
    private String allSongsJson = "["
                               	+ "{"
                            	+ 	"\"id\": 3,"
                            	+ 	"\"title\": \"Team\","
                            	+	"\"artist\": \"Iggy Azalea\","
                            	+	"\"released\": 2016"
                            	+"},"
                            	+"{"
                            	+	"\"id\": 2,"
                            	+	"\"title\": \"Mom\","
                            	+	"\"artist\": \"Meghan Trainor, Kelli Trainor\","
                            	+	"\"album\": \"Thank You\","
                            	+	"\"released\":  2016"
                            	+"},"
                            	+"{"
                            	+	"\"id\": 1,"
                            	+	"\"title\": \"Can’t Stop the Feeling\","
                            	+	"\"artist\": \"Justin Timberlake\","
                            	+	"\"album\": \"Trolls\","
                            	+	"\"released\": 2016"
                            	+"}"
                            	+"]";
    
    private String allSongsWithNewSongJson = "["
    		+ "{"
    		+ 	"\"id\": 4,"
        	+	"\"title\": \"test title\","
        	+	"\"artist\": \"test artist\","
        	+	"\"released\": 2018,"
        	+	"\"album\": \"test album\""
        	+"},"
           	+ "{"
        	+ 	"\"id\": 3,"
        	+ 	"\"title\": \"Team\","
        	+	"\"artist\": \"Iggy Azalea\","
        	+	"\"released\": 2016"
        	+"},"
        	+"{"
        	+	"\"id\": 2,"
        	+	"\"title\": \"Mom\","
        	+	"\"artist\": \"Meghan Trainor, Kelli Trainor\","
        	+	"\"album\": \"Thank You\","
        	+	"\"released\":  2016"
        	+"},"
        	+"{"
        	+	"\"id\": 1,"
        	+	"\"title\": \"Can’t Stop the Feeling\","
        	+	"\"artist\": \"Justin Timberlake\","
        	+	"\"album\": \"Trolls\","
        	+	"\"released\": 2016"
        	+"}"
        	+"]";
    
    
    private String oneSongJson = "{"
        	+	"\"id\": 1,"
        	+	"\"title\": \"Can’t Stop the Feeling\","
        	+	"\"artist\": \"Justin Timberlake\","
        	+	"\"album\": \"Trolls\","
        	+	"\"released\": 2016"
        	+"}";
    
    
    @Before
    public void setUp() throws ServletException {
    		objectMapper = new ObjectMapper();
        	servlet = new SongsStoreServlet();
        	config = mock(ServletConfig.class);
    }
    
    @SuppressWarnings("unchecked")
//	@Test
    public void playingWithJackson() throws IOException {
	    // songs.json and testSongs.json contain songs from this Top-10:
	    // http://time.com/collection-post/4577404/the-top-10-worst-songs/
	    
    	    // Read a JSON file and create song list:
    		InputStream input = this.getClass().getClassLoader().getResourceAsStream("songs.json");
	    
    		List<Song> songList = (List<Song>) objectMapper.readValue(input, new TypeReference<List<Song>>(){});
	    
	    	// write a list of objects to a JSON-String with prettyPrinter
	    	String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(songList);
	    
	    	// write a list of objects to an outputStream in JSON format
	    	objectMapper.writerWithDefaultPrettyPrinter().writeValue(new FileOutputStream("output.json"), songList);
    
	    	// Create a song and write to JSON
	    	Song song = new Song (null, "titleXX", "artistXX", "albumXX", 1999);
	    byte[] jsonBytes = objectMapper.writeValueAsBytes(song);
	    
	    // Read JSON from byte[] into Object
	    Song newSong = (Song) objectMapper.readValue(jsonBytes, Song.class);
	    assertEquals(song.getTitle(), newSong.getTitle());
	    assertEquals(song.getArtist(), newSong.getArtist());
    }
    

    
    @Test
    public void initShouldLoadSongList() throws ServletException {
		servlet.init(config);
		assertEquals(servlet.songStore.size(), 3);
		assertEquals(servlet.songStore.get(1).getId(), (Integer) 1);
		assertEquals(servlet.songStore.get(1).getTitle(), "Can’t Stop the Feeling");
    }

	@Test
    public void doGetWithParameterAllShouldReturnAllSongs() throws IOException, JSONException, ServletException {
		servlet.init(config);
		request = new MockHttpServletRequest("GET", "/songWebStore");
		response = new MockHttpServletResponse();
		request.addParameter("all", "");
		servlet.doGet(request, response);
		JSONAssert.assertEquals(allSongsJson, response.getContentAsString(), false);
		assertEquals(200, response.getStatus());
	}
	

	@Test
    public void doGetWithParameterSongIdShouldReturnSongWithId() throws ServletException, IOException, JSONException {	
		servlet.init(config);
		request = new MockHttpServletRequest("GET", "/songWebStore");
		response = new MockHttpServletResponse();
		request.addParameter("songId", "1");
		servlet.doGet(request, response);
		JSONAssert.assertEquals(oneSongJson, response.getContentAsString(), false);
		assertEquals(200, response.getStatus());
    }
	
	@Test
    public void doGetWithoutParameterShouldReturnBadRequest() throws IOException, JSONException, ServletException {
		servlet.init(config);
		request = new MockHttpServletRequest("GET", "/songWebStore");
		response = new MockHttpServletResponse();
		servlet.doGet(request, response);
		JSONAssert.assertEquals("{\"message\": \"Please specify 1 Get Parameter!\"}\n", response.getContentAsString(), false);
		assertEquals(401, response.getStatus());
    }
    
	@Test
    public void doGetWithWrongParameterShouldReturnBadRequest() throws ServletException, IOException, JSONException {
		servlet.init(config);
		request = new MockHttpServletRequest("GET", "/songWebStore");
		response = new MockHttpServletResponse();
		request.addParameter("blah", "1");
		servlet.doGet(request, response);
		JSONAssert.assertEquals("{\"message\": \"Parameter should be 'all' or 'songId'!\"}", response.getContentAsString(), false);
		assertEquals(401, response.getStatus());
    }
	
	@Test
    public void doGetWithWrongSongIdTypeShouldReturnBadRequest() throws ServletException, IOException, JSONException {
		servlet.init(config);
		request = new MockHttpServletRequest("GET", "/songWebStore");
		response = new MockHttpServletResponse();
		request.addParameter("songId", "test");
		servlet.doGet(request, response);
		JSONAssert.assertEquals("{\"message\": \"Parameter value needs to be of type int!\"}", response.getContentAsString(), false);
		assertEquals(401, response.getStatus());
    }
	
	@Test
    public void doGetWithWrongSongIdShouldReturnNotFound() throws ServletException, IOException, JSONException {
		servlet.init(config);
		request = new MockHttpServletRequest("GET", "/songWebStore");
		response = new MockHttpServletResponse();
		request.addParameter("songId", "4");
		servlet.doGet(request, response);
		JSONAssert.assertEquals("{\"message\": \"Could not find Song with Id: 4\"}", response.getContentAsString(), false);
		assertEquals(404, response.getStatus());
    }
    
    @Test
    public void doPostShouldCreateNewSong() throws ServletException, IOException {      
		servlet.init(config);
		request = new MockHttpServletRequest("POST", "/songWebStore");
		response = new MockHttpServletResponse();
		String requestBody = "{"
	        	+	"\"title\": \"test title\","
	        	+	"\"artist\": \"test artist\","
	        	+	"\"released\": 2018,"
	        	+	"\"album\": \"test album\""
	        	+"}";
		request.setContent(requestBody.getBytes("UTF-8"));
		servlet.doPost(request, response);
		assertEquals(200, response.getStatus());
    }
    
    //@Test
    public void doPostShouldWithoutContentShouldFail() throws ServletException, IOException, JSONException {      
		servlet.init(config);
		request = new MockHttpServletRequest("POST", "/songWebStore");
		response = new MockHttpServletResponse();
		servlet.doPost(request, response);
		assertEquals(401, response.getStatus());
		JSONAssert.assertEquals("{\"message\": \"Please send valid format!\"}", response.getContentAsString(), false);
		//Another get request
		request = new MockHttpServletRequest("GET", "/songWebStore");
		response = new MockHttpServletResponse();
		request.addParameter("all", "");
		servlet.doGet(request, response);
		JSONAssert.assertEquals(allSongsWithNewSongJson, response.getContentAsString(), false);
		assertEquals(200, response.getStatus());
    }
    
    @Test
    public void doPostShouldWithMissingParameterShouldFail() throws ServletException, IOException, JSONException {      
		servlet.init(config);
		request = new MockHttpServletRequest("POST", "/songWebStore");
		String requestBody = "{"
	        	+	"\"title\": \"test title\","
	        	+	"\"artist\": \"test artist\","
	        	+	"\"album\": \"test album\""
	        	+"}";
		request.setContentType("application/json");
		request.setContent(requestBody.getBytes("UTF-8"));
		response = new MockHttpServletResponse();
		servlet.doPost(request, response);
		assertEquals(401, response.getStatus());
		JSONAssert.assertEquals("{\"message\": \"Could not create Song\", \"missingFields\": [\"released\"]}", response.getContentAsString(), false);
    }
}
